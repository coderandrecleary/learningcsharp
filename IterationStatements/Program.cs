﻿using System;

namespace IterationStatements
{
    class Program
    {
        static void Main(string[] args)
        {
            // Looping with the foreach statement
            // The foreach statement is a bit different from the previous three iteration statements.

            //  It is used to perform a block of statements on each item in a sequence, for example, 
            //  an array or collection. Each item is usually read-only, and if the sequence structure is modified 
            //  during iteration, for example, by adding or removing an item, then an exception will be thrown.

            int x = 0;
            while (x < 10)
            {
                Console.WriteLine(x);
                x++;
            }


            string password = string.Empty;
            do
            {
                Console.Write("Enter your password: ");
                password = Console.ReadLine();
            }
            while (password != "Pa$$w0rd");
            Console.WriteLine("Correct!");



            string[] names = { "Adam", "Barry", "Charlie" };
            foreach (string name in names)
            {
                Console.WriteLine($"{name} has {name.Length} characters.");
            }


        }
    }
}