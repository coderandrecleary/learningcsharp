﻿using System;

/*Controlling Flow and Converting Types (CHAPTER 3) 
TOPICS COVERED IN THIS CHAPTER ARE:
1.) OPERATING ON VARIBLES 
2.) UNDERSTANDING SELECTION STATESMENTS
3.) UNDERSTANDING ITERATION STATEMENTS
4) CASTING AND CONVERTING BETWEEN TYPES
5) HANDLING EXCEPTIONS
6) CHECK FOR OVERFLOW
*/

namespace Chapter3
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
              OPERATIONS ON VARIBLES
              Some operators are unary, meaning they work on a single operand, 
              and can apply before or after the operand.            
             */


            int x = 5;
            int incrementedByOne = x++;
            int incrementedByOneAgain = ++x;
            /*
            The typeof is an operator keyword which is used to get a type at the compile-time. 
            Or in other words, this operator is used to get the System.Type object for a type. 
            This operator takes the Type itself as an argument and returns the marked type of the argument.

            Important points:

            The operand of typeof operator is always a type of parameter or name of the type. It does not contain variable.
            It is not allowed to overload typeof operator.
            It is allowed to use typeof operator on open generic types.
            It is allowed to use typeof operator on bounded or unbounded types.

            Syntax:    System.Type type = typeof(int);
            //  */
            Type theTypeOfAnInteger = typeof(int);
            int howManyBytesInAnInteger = sizeof(int);

            Console.WriteLine(incrementedByOne);
            Console.WriteLine(incrementedByOneAgain);
            Console.WriteLine(theTypeOfAnInteger);
            Console.WriteLine(howManyBytesInAnInteger);
            Console.WriteLine(x);







        }
    }
}
