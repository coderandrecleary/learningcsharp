﻿using System;


namespace Operators
{
    class Program
    {
        static void Main(string[] args)
        {

            int a = 3;
            int b = a++;
            Console.WriteLine($"a is {a}, b is {b}");
            // a is 4, b is 3
            /*
               The variable b has the value 3 because the ++ operator executes after the assignment; 
               this is known as a postfix operator. 
               If you need to increment before the assignment, then use the prefix operator.
            */
            double g = 11.0;
            int f = 3;
            Console.WriteLine($"g is {g:N1}, f is {f}");
            Console.WriteLine($"g / f = {g / f}");


            int e = 11;
            // int f = 3;
            Console.WriteLine($"e is {e}, f is {f}");
            Console.WriteLine($"e + f = {e + f}");
            Console.WriteLine($"e - f = {e - f}");
            Console.WriteLine($"e * f = {e * f}");
            Console.WriteLine($"e / f = {e / f}");
            Console.WriteLine($"e % f = {e % f}");



            // ASSIGNMENT OPERATORS:
            int p = 6;
            p += 3; // equivalent to p = p + 3;
            p -= 3; // equivalent to p = p - 3;
            p *= 3; // equivalent to p = p * 3;
            p /= 3; // equivalent to p = p / 3;

        }
    }
}
