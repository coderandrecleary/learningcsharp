﻿using System;

namespace Packt.Shared
{
    public class Person : object
    {
        public string Name;
        public DateTime DateOfBirth;

        public static void Main(String[] args)
        {

            // fields CONSTANTS, READ-ONLY AND EVENTS


            var alice = new Person
            {
                Name = "Alice Jones",
                DateOfBirth = new DateTime(1998, 3, 7)
            };
            Console.WriteLine(
              format: "{0} was born on {1:dd MMM yy}",
                      arg0: alice.Name,
                      arg1: alice.DateOfBirth);

            // Bob Smith was born on Wednesday, 22 December 1965
            // Alice Jones was born on 07 Mar 98

        }



        //writing and calling methods
        public (string, int) GetFruit()
        {
            return ("Apples", 5);
        }

        // methods
        public void WriteToConsole()
        {
            Console.WriteLine($"{Name} was born on a {DateOfBirth:dddd}.");
        }


        public class TextAndNumber
        {
            public string Text;
            public int Number;
        }
        public class Processor
        {
            public TextAndNumber GetTheData()
            {
                return new TextAndNumber
                {
                    Text = "What's the meaning of life?",
                    Number = 42
                };
            }
        }

        // Deconstructing is ThreadStaticAttribute use of tuples 
        // as EnvironmentVariableTarget itself instead of a varible name.



        //         Overloading methods
        // Instead of having two different method names, we could give both methods the same name.
        // This is allowed because the methods each have a different signature.
        // A method signature is a list of parameter types that can be passed when calling the method (as well as the type of the return value).






        public enum WondersOfTheAncientWorld//USING ENUM FOR MULTIPLE TYPES OF SIMILAR OBJECT
        {
            GreatPyramidOfGiza,
            HangingGardensOfBabylon,
            StatueOfZeusAtOlympia,
            TempleOfArtemisAtEphesus,
            MausoleumAtHalicarnassus,
            ColossusOfRhodes,
            LighthouseOfAlexandria
        }



        //         Controlling how parameters are passed
        // When a parameter is passed into a method, it can be passed in one of three ways:

        // By value(this is the default): Think of these as being in-only.
        // By reference as a ref parameter: Think of these as being in-and-out.
        // As an out parameter: Think of these as being out-only.




        // In the Person class, add statements to define a method with three parameters, one in parameter, one ref parameter, 
        // and one out parameter, as shown in the following method:
        public void PassingParameters(int x, ref int y, out int z)
        {
            // out parameters cannot have a default
            // AND must be initialized inside the method 
            z = 99;
            // increment each parameter 
            x++;
            y++;
            z++;
        }
        //////////////////////////////////////////////////////////

        //When passing a variable as a parameter by default, its current value gets passed, not the variable itself.
        //Therefore, x is a copy of the a variable. The a variable retains its original value of 10.
        //When passing a variable as a ref parameter, a reference to the variable gets passed into the method.

        // Therefore, y is a reference to b. The b variable gets incremented when the y parameter gets incremented.
        //  When passing a variable as an out parameter, a reference to the variable gets passed into the method.

        // Therefore, z is a reference to c. The c variable gets replaced by whatever code executes inside the method. 
        // We could simplify the code in the Main method by not assigning the value 30 to the c variable, since it will always be replaced anyway.

        // In C# 7.0 and later, we can simplify code that uses the out variables.

        // int a = 10;
        // int b = 20;
        // int c = 30;
        // Console.WriteLine($"Before: a = {a}, b = {b}, c = {c}");
        // bob.PassingParameters(a, ref b, out c);
        // Console.WriteLine($"After: a = {a}, b = {b}, c = {c}");

        ////////////////////////////////////////////////////////////


        // Passing optional parameters and naming arguments
        // Another way to simplify methods is to make parameters optional. You make a parameter optional by 
        // assigning a default value inside the method parameter list. Optional parameters must always come 
        // last in the list of parameters.
        public string OptionalParameters(
        string command = "Run!",
        double number = 0.0,
        bool active = true)
        {
            return string.Format(
              format: "command is {0}, number is {1}, active is {2}",
              arg0: command, arg1: number, arg2: active);
        }



        //BUILDING CLASS LIBRARIES

        /*
            FOR OBJECT ORIENTED PROGRAMMING: CHAPTER 5
            ENCAPSULATION
            COMPOSITION
            AGGREGATION
            INHERITANCE
            POLYMORPHISM
            ABSTRACTION

         */




        /*
            METHODS:
            CONSTRUCTOR
            PROPERTY
            INDEXER
            OPERATOR
         */



        // Access Modifier:

        // private  Member is accessible inside the type only. This is the default.

        // internal Member is accessible inside the type and any type in the same assembly.

        // protected Member is accessible inside the type and any type that inherits from the type.

        // public 	Member is accessible everywhere.

        // internal protected Member is accessible inside the type, 
        //any type in the same assembly, 
        //and any type that inherits from the type. Equivalent to a fictional access modifier named internal_or_protected.


        // private protected Member is accessible inside the type, or any type that inherits from the type and is in the same assembly. 
        //Equivalent to a fictional access modifier named internal_and_protected. This combination is only available with C# 7.2 or later.




        //         Splitting classes using partial
        // When working on large projects with multiple team members, it is useful to be able to split the definition of a complex class across multiple files.
        // You do this using the partial keyword.

        // Imagine we want to add statements to the Person class that are automatically generated by a tool like an object-relational mapper that reads schema information from a database.
        // If the class is defined as partial, then we can split the class into an auto-generated code file and a manually edited code file.


        namespace Packt.Shared
    {
        public partial class Person
        { }

    }
}
