﻿using System;
using static System.Convert;


namespace CastingConverting
{
    class Program
    {
        static void Main(string[] args)
        {
            //             /*
            //                 SPECIFIC EXCEPTIONS CAN BE PLACED AS PARAMETERS FOR THE CATCH BLOCK... 
            //              */
            //             try
            //             {

            //             }
            //             catch (OverflowException)
            //             {
            //                 Console.WriteLine("Your age is a valid number format but it is either too big or small.");
            //             }
            //             catch (FormatException)
            //             {
            //                 Console.WriteLine("The age you entered is not a valid number format.");
            //             }





            //             double[] doubles = new[]
            //               { 9.49, 9.5, 9.51, 10.49, 10.5, 10.51 };
            //             foreach (double n in doubles)
            //             {
            //                 Console.WriteLine($"ToInt({n}) is {ToInt32(n)}");
            //             }
            //             /*
            //                 The following example demonstrates some of the conversion 
            //                 methods in the Convert class, including #######ToInt32, ToBoolean, and ToString.######
            //                 (METHODS IN THE CONVERT CLASS)

            //              */
            //             double dNumber = 23.15;

            //             try
            //             {
            //                 // Returns 23
            //                 int iNumber = System.Convert.ToInt32(dNumber);
            //             }
            //             catch (System.OverflowException)
            //             {
            //                 System.Console.WriteLine(
            //                             "Overflow in double to int conversion.");
            //             }
            //             // Returns True
            //             bool bNumber = System.Convert.ToBoolean(dNumber);

            //             // Returns "23.15"
            //             string strNumber = System.Convert.ToString(dNumber);

            //             try
            //             {
            //                 // Returns '2'
            //                 char chrNumber = System.Convert.ToChar(strNumber[0]);
            //             }
            //             catch (System.ArgumentNullException)
            //             {
            //                 System.Console.WriteLine("String is null");
            //             }
            //             catch (System.FormatException)
            //             {
            //                 System.Console.WriteLine("String length is greater than 1.");
            //             }

            //             // System.Console.ReadLine() returns a string and it
            //             // must be converted.
            //             int newInteger = 0;
            //             try
            //             {
            //                 System.Console.WriteLine("Enter an integer:");
            //                 newInteger = System.Convert.ToInt32(
            //                                     System.Console.ReadLine());
            //             }
            //             catch (System.ArgumentNullException)
            //             {
            //                 System.Console.WriteLine("String is null.");
            //             }
            //             catch (System.FormatException)
            //             {
            //                 System.Console.WriteLine("String does not consist of an " +
            //                                 "optional sign followed by a series of digits.");
            //             }
            //             catch (System.OverflowException)
            //             {
            //                 System.Console.WriteLine(
            //                 "Overflow in string to int conversion.");
            //             }

            //             System.Console.WriteLine("Your integer as a double is {0}",
            //                                      System.Convert.ToDouble(newInteger));






            //             /*
            //                Add statements to the bottom of the Main method
            //                to declare and assign 
            //                a value to a double variable,
            //                convert it to an integer,
            //                and then write both values to the console, as shown in the following code: 

            //                              */
            //             int h = ToInt32(g);
            //             double g = 9.8;
            //             Console.WriteLine($"g is {g} and h is {h}");
            //             /*
            //                        VERY IMPORTANT POINT TO NOTE!!!: One difference between casting
            //                        and converting is that converting rounds the double value 9.8 up
            //                        to 10 instead of trimming the part after the decimal point.


            //                 One difference between casting and converting is that converting 
            //                 rounds the double value 9.8 up to 10 instead of trimming the part after the decimal point.
            //              */


            //             int a = 10;
            //             double b = a; // an int can be safely cast into a double
            //             Console.WriteLine(b);


            //             double c = 9.8;
            //             int d = (int)c; // compiler gives an error for this line
            //             Console.WriteLine(d);

            int x = int.MaxValue - 1;
            Console.WriteLine($"Initial value: {x}");
            x++;
            Console.WriteLine($"After incrementing: {x}");
            x++;
            Console.WriteLine($"After incrementing: {x}");
            x++;
            Console.WriteLine($"After incrementing: {x}");





            checked
            {
                int a = int.MaxValue - 1;
                Console.WriteLine($"Initial value: {a}");
                a++;
                Console.WriteLine($"After incrementing: {a}");
                a++;
                Console.WriteLine($"After incrementing: {a}");
                a++;
                Console.WriteLine($"After incrementing: {a}");
            }
        }
    }
}
